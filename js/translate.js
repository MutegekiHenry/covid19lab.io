function translateTextContent() {

    if ( typeof dictionary_text === 'undefined' ) {

        return false;

    }

    var elems = document.querySelectorAll( '[data-transtext]' );
    var elemsNum = elems.length;

    for ( var i = 0 ; i < elemsNum ; i++ ) {

        var textToTranslate = elems[ i ].getAttribute( 'data-transtext' ); //console.log( textToTranslate ); console.log( text_el_GR[ textToTranslate ] );

        if ( dictionary_text.hasOwnProperty( textToTranslate ) ) { //console.log( elems[ i ] );

            elems[ i ].textContent = dictionary_text[ textToTranslate ];

        }

    }

}

function translateDisclaimer() {

    if ( typeof dictionary_disclaimer === 'undefined' ) {

        return false;

    }

    document.getElementById( 'disclaimer' ).innerHTML = dictionary_disclaimer;

}

function translateTitles() {

    if ( typeof dictionary_text === 'undefined' ) {

        return false;

    }

    var elems = document.querySelectorAll( '[title]' );
    var elemsNum = elems.length;

    for ( var i = 0 ; i < elemsNum ; i++ ) {

        var textToTranslate = elems[ i ].getAttribute( 'title' );

        if ( dictionary_text.hasOwnProperty( textToTranslate ) ) {

            elems[ i ].setAttribute( 'title', dictionary_text[ textToTranslate ] );

        }

    }

}

function translateCountry( country_name ) {

    if ( typeof dictionary_countries === 'undefined' ) {

        return country_name;

    }

    if ( dictionary_countries.hasOwnProperty( country_name ) === false ) {

        return country_name;

    }

    return dictionary_countries[ country_name ];

}

function translateText( text ) {

  if ( typeof dictionary_text === 'undefined' ) {

      return text;

  }

  if ( dictionary_text.hasOwnProperty( text ) === false ) {

      return text;

  }

  return dictionary_text[ text ];

}

function click_languages( event ) {

    if ( event.target.classList.contains( 'active' ) === true ) {

        event.target.classList.remove( 'active' );
        document.getElementById('languages').style.display = 'none';

    } else {

      event.target.classList.add( 'active' );
      document.getElementById('languages').style.display = 'block';

    }

}

function bindLanguagesButton() {

    if ( document.getElementById( 'pickLanguage' ) !== null ) {

        document.getElementById( 'pickLanguage' ).addEventListener( 'click', click_languages );

    }

}

translateTextContent();

translateDisclaimer();

translateTitles();

bindLanguagesButton();
